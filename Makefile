NOTES_DIR = notes
NOTES = $(foreach NOTE,$(wildcard $(NOTES_DIR)/*.org),$(shell awk 'BEGIN {file="$(NOTE)";gsub("^notes/[0-9]*-","",file) ;print file;}'))

HUGO_DIR = hugo
HUGO_CONTENT_DIR = $(HUGO_DIR)/content
HUGO_NOTES_DIR = $(HUGO_CONTENT_DIR)/notes

URL_PATH_PREFIX = /roam/notes

.DEFAULT_GOAL = all

.PHONY: print-vars
print-vars:
	@echo "NOTES_DIR = $(NOTES_DIR)"
	@echo "NOTES = $(NOTES)"
	@echo "HUGO_CONTENT_DIR = $(HUGO_CONTENT_DIR)"
	@echo "HUGO_NOTES_DIR = $(HUGO_NOTES_DIR)"

.PHONY: all
all: hugo-notes
	cd $(HUGO_DIR) && hugo

.PHONY: serve
serve: hugo-notes
	cd $(HUGO_DIR) && hugo server -D

run-pipeline:
	docker run --pull --rm -it registry.gitlab.com/pages/hugo/hugo_extended:latest sh

# .PHONY: publish
# publish:
# 	emacs --batch --quick --load publish.el --funcall publish

.PHONY: hugo-notes
hugo-notes: $(foreach NOTE,$(NOTES),$(HUGO_NOTES_DIR)/$(NOTE))
	$(eval ID_MAPPING := $(shell cd $(HUGO_NOTES_DIR) && grep -E '^:ID:\s*[a-z0-9-]+$$' *.org | sed -E "s|^([^.]+)[.]org::ID:\s*([^\s]+)|-e 's!id:\2!$(URL_PATH_PREFIX)/\1/!g'|" | tr -s '\n' ' ' ) )

	sed -i $(ID_MAPPING) $(HUGO_NOTES_DIR)/*.org
	sed -i '/^:PROPERTIES:$$/,/^:END:$$/d' $(HUGO_NOTES_DIR)/*.org

$(HUGO_NOTES_DIR):
	mkdir -p $@

# Copies org notes inside hugo directory.
# # find notes/ -name '*-golang_scripting.org'
#$(HUGO_NOTES_DIR)/%: $(NOTES_DIR)/*% | $(HUGO_NOTES_DIR)
$(HUGO_NOTES_DIR)/%.org: | $(HUGO_NOTES_DIR)
	$(eval SOURCE := $(wildcard $(NOTES_DIR)/*-$(notdir $@)))
	cp $(SOURCE) $@

.PHONY: clean
clean:
	rm -rf $(HUGO_NOTES_DIR)/*
