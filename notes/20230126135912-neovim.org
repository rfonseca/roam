:PROPERTIES:
:ID:       2e7de333-a3fc-4457-acc2-fd24554a7be3
:END:
#+title: NeoVim

- tags :: [[id:c44de5b8-e25a-409e-a028-04c9099f895e][Code Editors]]

* What is it?
A modern fork of [[id:9be6c8bd-6ad7-444a-9bfd-27326695cdd8][Vim]].

One of the main features is that it is completely configurable in [[id:30bc0f7a-4230-48d7-8d4d-d59998583018][Lua]] script.

- https://neovim.io/

* Configuration
** Lazyvim
#+begin_quote
LazyVim is a Neovim setup powered by zzz lazy.nvim to make it easy to customize and extend your config. Rather than having to choose between starting from scratch or using a pre-made distro, LazyVim offers the best of both worlds - the flexibility to tweak your config as needed, along with the convenience of a pre-configured setup.
#+end_quote
- https://github.com/LazyVim/LazyVim
