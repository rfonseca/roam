:PROPERTIES:
:ID:       609fe4f4-fb88-437c-a7ab-d16b1f3d3291
:END:
#+title: Istio

- tags :: [[id:886415b5-fd38-428f-a6ba-93c20bae23fc][Kubernetes]] [[id:e2f4beb7-2ec8-4adb-8f93-dad4e373bcce][Service Mesh]]

* Resources
** Istiops
A CLI for managing deploy strategies
  https://www.infoq.com/br/presentations/estrategias-deploy-istio-com-golang/
