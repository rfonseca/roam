:PROPERTIES:
:ID:       5cb982b7-a293-47af-b900-fd0dd14371b5
:END:
#+title: Working Asynchronous

- tags :: [[id:ed778b88-0c9f-4378-8454-3db2692dea7c][Software Development]]

GitLab has an amazing [[https://about.gitlab.com/company/culture/all-remote/asynchronous][Handbook]] on how to work asynchronous.

Some highlights:

 - Every meet should be a [[https://about.gitlab.com/handbook/values/#make-a-proposal][review of a proposal]];
 - [[https://about.gitlab.com/company/culture/all-remote/management/#scaling-by-documenting][Scaling by documenting]];
 - Use more issue trackers (issues, epic or merge request);
 - Meetings invites should have a [[https://about.gitlab.com/handbook/communication/#scheduling-meetings][document]] attached for communication and interaction;

They use some external tools like: [[https://www.loom.com/][loom]] to create short videos.
