:PROPERTIES:
:ID:       9be6c8bd-6ad7-444a-9bfd-27326695cdd8
:END:
#+title: Vim

* What is?
An ok editor with an amazing mode
* Tips
** Zip and encode a file into base 64
#+begin_src vim
:'<,'>!gzip | base64 -w 0
#+end_src
** Formating json with jq
#+begin_src vim
:%!jq '.'
#+end_src

Resource: https://til.hashrocket.com/posts/ha0ci0pvkj-format-json-in-vim-with-jq
