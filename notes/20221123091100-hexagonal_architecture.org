:PROPERTIES:
:ID:       98a58961-ab98-4f24-b5d1-7bf552986fa1
:END:
#+title: Hexagonal Architecture

- tags :: [[id:a6c41594-d22b-4c2c-9a6f-edfdcf308968][Software Architecture]]

* Hexagonal Architecture

** Ports and adapters
This [[https://herbertograca.com/2017/09/14/ports-adapters-architecture/][article]] explains about ports and adapters.
