:PROPERTIES:
:ID:       5d6ca059-11bd-4b3c-8aa3-e477a2216faf
:END:
#+title: BTRFS

- tags :: [[id:9acd3fa7-6259-4d0f-8cc3-01b87da979d9][Linux]]

* What is this?
A new Filysystem that could be the new linux default.

It allows for fast and easy snapshots and subvolumes.

** Snapper
A snapshot tool for automate taking snapshots.

[[http://snapper.io/manpages/snapper.html][snapper]]
[[http://snapper.io/manpages/snapper-configs.html][snapper-configs]]
[[https://github.com/Antynea/grub-btrfs][GitHub - Antynea/grub-btrfs: Include btrfs snapshots at boot options. (Grub m...]]
