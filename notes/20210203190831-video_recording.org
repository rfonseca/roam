:PROPERTIES:
:ID:       7226eb91-5116-4e18-aada-4e975661e002
:END:
#+title: Video Recording

- tags :: [[id:5cb982b7-a293-47af-b900-fd0dd14371b5][Working Asynchronous]]

Recording videos are a great way to share ideas with many people without the need to have everybody on the same room.

Some useful tools are:

* OBS
Great tool for recording yourself and the computer screen or apps.
Audio is horrible, you must configure audio filters.

What filters worked for me (in this order):
1. Noise Suppression, with RNNoise
2. Compressor, with default
3. Gain, with 6dB (enough to get my audio to peak at -3dB)
4. Limiter, with -3dB

Tip: Draw over the video.
 Open an image drawer like pinta.
 Paint the background in green
 Add a screen capture of the app on top of the video
 Use the chomakey effect
 Draw!
* ScreenKey
This shows which keys are being pressed.
This could be used for console demonstrations.

* Peek
Simple and easy tool to record parts of the screen and create animated gifs.
