:PROPERTIES:
:ID:       34980b44-eb63-400c-aecb-00098cbdc902
:END:
#+title: tmux

- tags :: [[id:9acd3fa7-6259-4d0f-8cc3-01b87da979d9][Linux]]

** Run script in detached tmux
#+begin_src sh
tmux new-session -d -s my_session 'ruby run.rb'
#+end_src
I read this on [[https://stackoverflow.com/questions/31902929/how-to-write-a-shell-script-that-starts-tmux-session-and-then-runs-a-ruby-scrip][this]] stack overflow post.
