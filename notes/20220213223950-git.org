:PROPERTIES:
:ID:       704dcda3-62d4-4b97-a977-01c81e190d52
:END:
#+title: Git

- tags :: [[id:9acd3fa7-6259-4d0f-8cc3-01b87da979d9][Linux]]

* Writing good commit messages
[[https://dev.to/i5han3/git-commit-message-convention-that-you-can-follow-1709][Git commit message convention that you can follow! - DEV Community]]
[[https://cbea.ms/git-commit/][How to Write a Git Commit Message]]
