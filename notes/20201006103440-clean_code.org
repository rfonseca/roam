:PROPERTIES:
:ID:       18aeb48e-9860-4106-b5c5-e7d25f6a96f5
:END:
#+title: Clean Code

- tags :: [[id:ed778b88-0c9f-4378-8454-3db2692dea7c][Software Development]]

A nice way to write code. Checkout the [[https://blog.cleancoder.com][blog]].

Unfortunate that it's most in Java.
* Some articles from the blog
https://blog.cleancoder.com/uncle-bob/2013/05/27/TheTransformationPriorityPremise.html

I've read this [[https://itnext.io/golang-and-clean-architecture-19ae9aae5683][article]] about clean code in Go. It has some nice ideas, but I still dislike the single entity package and how polluted the root directory is.
