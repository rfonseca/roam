:PROPERTIES:
:ID:       1262e7bc-3b0b-4770-a48d-9d03a83b9302
:END:
#+title: Architecture Decision Record (ADR)

- tags :: [[id:a6c41594-d22b-4c2c-9a6f-edfdcf308968][Software Architecture]] [[id:bf974dde-86a2-493e-af87-25e25c888c69][Documentation]]

* Markdown ADRs

- [[https://github.com/adr/madr][GitHub - adr/madr: Markdown Architectural Decision Records]]
- [[https://adr.github.io/madr/][Markdown Any Decision Records - Any Decisions Records]]

- [[https://jon.sprig.gs/blog/post/2339][Cross Platform Decision Records/Architectural Decision Records – a HowTo Guid...]]
** ADR Tools

- [[https://www.npmjs.com/package/adr-tools][adr-tools - npm]]

* Links
- [[https://github.com/joelparkerhenderson/architecture-decision-record][GitHub - joelparkerhenderson/architecture-decision-record: Architecture decis...]]
  - https://github.com/joelparkerhenderson/architecture-decision-record/blob/main/templates/decision-record-template-by-michael-nygard/index.md
  - https://raw.githubusercontent.com/joelparkerhenderson/architecture-decision-record/main/templates/decision-record-template-madr/index.md
- [[https://www.industrialempathy.com/posts/design-docs-at-google/][Design Docs at Google]]
- [[https://multithreaded.stitchfix.com/blog/2020/12/07/remote-decision-making/][Technical Decision-Making and Alignment in a Remote Culture | Stitch Fix Tech...]]
- [[https://docs.devland.is/technical-overview/adr/0000-use-markdown-architectural-decision-records][Use Markdown Architectural Decision Records - Handbook]]

An ADR considering other ADRs but choosing MADR: [[https://toscana.readthedocs.io/en/latest/dev/adr/0000-use-markdown-architectural-decision-records/][0000 - Use Markdown Architectural Decision Records - TOSCAna]]
[[https://adr.github.io/madr/][Markdown Any Decision Records - Any Decisions Records]][[https://www.npmjs.com/package/adr-tools][adr-tools - npm]]
