:PROPERTIES:
:ID:       1b1592ea-6867-455e-8509-6f87f33f00d4
:END:
#+title: Technology Radar
* What is this?
Every now and then thoughtworks do a survey/study on the main tecnologies and

* Previous radars
** 2022
[[https://www.thoughtworks.com/content/dam/thoughtworks/documents/radar/2022/10/tr_technology_radar_vol_27_en.pdf][This]] is the radar in end of 2022.

Some of my high lights

- [[https://dannorth.net/2022/02/10/cupid-for-joyful-coding/][CUPID]]
- [[https://github.com/OpenSLO/OpenSLO][OpenSLO]]
- [[https://backstage.io/][Backstage]]
- [[https://docs.greatexpectations.io/docs/][https://Great Expectations]]
- [[https://k6.io/][K6]]
- [[https://github.com/GoogleContainerTools/kaniko][Kaniko]]
* Idea
Build a radar using [[https://github.com/zalando/tech-radar][GitHub - zalando/tech-radar: Visualizing our technology choices]] and [[https://gohugo.io/][The world’s fastest framework for building websites | Hugo]].
