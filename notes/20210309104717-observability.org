:PROPERTIES:
:ID:       ecd49a96-1edf-4d8a-9234-cf41a0e1ff76
:END:
#+title: Observability

- tags :: [[id:ed778b88-0c9f-4378-8454-3db2692dea7c][Software Development]]

* What is?

Observability is the ability to understand what is happening inside of a system from the knowledge of its external outputs.

Learn more on:
- [[https://www.infoq.com/observability/][InfoQ]] has lots of videos, podcasts and articles;
- This blog post of [[https://www.dynatrace.com/news/blog/what-is-observability/][dynatrace]];
- This glossary entry on [[https://www.split.io/glossary/observability/][split.io]].
