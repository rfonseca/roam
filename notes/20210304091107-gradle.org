:PROPERTIES:
:ID:       fc9ca709-5023-481d-95db-29e63e0edfaa
:END:
#+title: Gradle

- tags :: [[id:af960016-3f7b-4190-bfe0-feb04dfe0e32][Scala]] [[id:7b71af63-1be0-41fd-9a24-83093747b127][Java]]

A build system for scala (and other java based languages) better than sbt.

https://engineering.linkedin.com/blog/2018/07/how-we-improved-build-time-by-400-percent
