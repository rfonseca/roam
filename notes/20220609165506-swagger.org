:PROPERTIES:
:ID:       0a84284f-9bf5-478b-a95c-269ea30fbbfb
:END:
#+title: Swagger

- tags :: [[id:bf974dde-86a2-493e-af87-25e25c888c69][Documentation]]

* Formating
- https://www.baeldung.com/swagger-format-descriptions

* Embeddable Swagger UI in [[id:5bc1ac0b-6e1a-4250-8fae-03aa93d421b9][Golang]]
- https://github.com/flowchartsman/swaggerui
