:PROPERTIES:
:ID:       b2d144d4-8208-4285-bae3-307441a03fdc
:END:
#+title: The Smartest Code Architecture

- tags :: [[id:ed778b88-0c9f-4378-8454-3db2692dea7c][Software Development]]

https://drpicox.medium.com/the-smartest-code-architecture-84b8277d4763

#+begin_quote
In TDD tests, units are units of behavior, not units of code.
#+end_quote

#+begin_quote
Because the smart architecture is not having an architecture, but leaving the options open until you know the exact fit.
#+end_quote
