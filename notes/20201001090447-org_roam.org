:PROPERTIES:
:ID:       409424fc-0229-472c-90ad-010e3ae0d5e4
:END:
#+title: org roam

- tags :: [[id:80500fed-5ccf-4f5b-b08f-0d4a4826201b][note taking]] [[id:0a87585f-3854-4738-b8d8-626158c05686][Org Mode]]

#+begin_quote
Org-roam is a Roam replica built on top of the all-powerful Org-mode.
#+end_quote

Some other people public projects:
- https://github.com/randomwangran/randomwangran
- https://gitlab.com/ngm/commonplace
Other links:
- [[https://www.orgroam.com/manual/index.html][manual]]
- github

Nice blog post about it
https://blog.jethro.dev/posts/how_to_take_smart_notes_org/

Articles

- https://commonplace.doubleloop.net/publishing-org-roam-via-gitlab-ci
- https://commonplace.doubleloop.net/preparing-for-org-roam-v2
- https://notes.baty.net/notes/publishing-portions-of-my-org-roam-database/
