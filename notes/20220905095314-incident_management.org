:PROPERTIES:
:ID:       cafc9cd9-13f1-4aea-9e54-39b9765e9ac9
:END:
#+title: Incident Management

* What is it?
#+begin_quote
An incident is anything that takes you away from planned work with a degree of urgency.
-- incident.io
#+end_quote

* Key Metrics

These metrics are used as part of the [[id:9f010e13-0a96-4886-9e29-25b72f0c10b8][4 Key Metrics]].

[[https://www.logicmonitor.com/blog/whats-the-difference-between-mttr-mttd-mttf-and-mtbf][What's the Difference Between MTTR, MTTD, MTTF, and MTBF? | LogicMonitor]]
** MTTA - Mean Time to Acknowledge
~MTTA = (total time to acknowledge detected failures) / (# of failures)~
#+begin_quote
average time from when a failure detected, to work beginning on the issue
#+end_quote
** MTTR - Mean Time to Recovery
~MTTR = (total time spent discovery & repairing) / (# of repairs)~
#+begin_quote
time it takes from when something goes down to the time that is back and at full functionality
#+end_quote
** MTTD - Mean Time to Detection
#+begin_quote
average time it takes you, or more likely a system, to realize that something has failed.
#+end_quote

* Links
https://sre.google/
[[https://incident.io/guide/foundations/defining-an-incident/][Defining an incident - Incident Management Guide]]
[[https://sre.google/sre-book/managing-incidents/][Google - Site Reliability Engineering]]
[[https://sre.google/workbook/incident-response/][Google - Site Reliability Engineering]]
[[https://www.atlassian.com/incident-management/incident-communication][Incident communication best practices | Atlassian]]
[[https://www.atlassian.com/incident-management/handbook#who-is-this-guide-for][The Atlassian Incident Management Handbook | Atlassian]]
[[https://sciencelogic.com/blog/mttr-vs-mttar-vs-mttd-incident-management-metrics][Mean Time to Repair (MTTR), MTTA & MTTD - ScienceLogic]]
