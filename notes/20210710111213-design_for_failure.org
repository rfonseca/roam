:PROPERTIES:
:ID:       55f83d35-2b3a-4966-abf5-038c5f644870
:END:
#+title: Design for Failure

- tags :: [[id:ed778b88-0c9f-4378-8454-3db2692dea7c][Software Development]]

* What is this?
Software should handle errors gracefully.

#+begin_quote
Designing for failure yields a self-healing infrastructure that acts with the maturity that is expected of recent workloads. [fn:1]
#+end_quote

* Key factors
** Disposability
Fast startup and graceful shutdown.

Maybe to think the workload as a /"Crash-only Software"/[fn:3] would be nice here. This is also the motto of Erlang[fn:4][fn:5] and can be be applied outside the language[fn:6]. This is a form of /Defensive Programming/[fn:7].
** Logs (as an event streaming)
For understanding when the software failed and why.
** Dev/Prod parity
Keep development and productions environment as similar as possible.

The dev/prod parity is also a factor of the [[id:0b91d8ea-8f68-4932-b266-091c65916273][The Twelve-Factor App]]
* Handling failure on other services
** Circuit Breakers
/TODO: Write a note about CB/

Circuit Breakers are important to allow for software to keep running on a degraded state or gracefully recovers from
** Retries
Errors 5xx and throttling errors should be retried[fn:2].


[fn:1] [[https://docs.aws.amazon.com/whitepapers/latest/running-containerized-microservices/design-for-failure.html][Design for Failure - Running Containerized Microservices on AWS]]
[fn:2] [[https://docs.aws.amazon.com/general/latest/gr/api-retries.html][Error retries and exponential backoff in AWS - AWS General Reference]]
[fn:3] [[https://lwn.net/Articles/191059/][Crash-only software: More than meets the eye {LWN.net}]]
[fn:4] [[https://dev.to/serokell/is-erlang-worth-learning-in-2021-28h2][Erlang: The Programming Language That Quietly Powers WhatsApp and WeChat - DE...]]
[fn:5] [[https://verraes.net/2014/12/erlang-let-it-crash/][Let It Crash]]
[fn:6] [[http://stratus3d.com/blog/2020/01/20/applying-the-let-it-crash-philosophy-outside-erlang/][The Let It Crash Philosophy Outside Erlang - Stratus3D]]
[fn:7] [[https://en.wikipedia.org/wiki/Defensive_programming][Defensive programming - Wikipedia]]
