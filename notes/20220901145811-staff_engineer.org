:PROPERTIES:
:ID:       2575fdce-ef0b-4622-a34d-6a9f93fb684b
:END:
#+title: Staff Engineer

* What is it?

** The 4 archetypes
*** Tech Lead
Acts as a TechLead on high impact projects.
*** Architect

- [[https://roadmap.sh/software-architect][Software Architect Roadmap]]
*** Solver
Works os specific problems, switching projects after it has been fixed.
*** Right-hand
Also makes executive decisions on behalf of the CEO. Helps sharing the CEO load.

** Takeaways
Bridge business and technology inside a company.
Helps implementing and improving the [[id:9f010e13-0a96-4886-9e29-25b72f0c10b8][4 Key Metrics]].

Helps [[id:3f612bcb-2a93-4aea-8f60-2b7c5f2bd28f][Mentoring]] other people.
* Links

- [[https://staffeng.com/][Stories of reaching Staff-plus engineering roles - StaffEng | StaffEng]]
- [[https://leaddev.com/staff-engineers-path-guide-individual-contributors-navigating][LeadDev The Staff Engineer’s Path: Tanya Reilly in conversation]]

* Podcast
** Escola Forja (Portuguese)
[[https://www.youtube.com/watch?v=RR-9xQsegDU][Ep. 1: O que vem depois de Dev Sênior? - YouTube]]

* Writting an Engineering Strategy
- [[https://lethain.com/eng-strategies/][Writing an engineering strategy. | Irrational Exuberance]]

* Job Description Canvas
** Desafios & Indicadores
#+begin_quote
Quais são os maiores desafios desse papel ou desafios em que estará envolvido?
Que indicadores podem ser impactados pela sua atuação?
#+end_quote
Desafios:
 - Atuar em problemas complexos ou que exigem escalabilidade
   #+begin_quote
   (arquitetar soluções escaláveis para problemas complexos)
   (problemas não resolvidos / solução não é clara / time não tem conhecimento)
   #+end_quote
 - Ajudar a definir a estratégia técnica da empresa
 - Trabalhar com diversos times diferentes ajudando a validar ideias e arquiteturas
 - Colaborar para confiabilidade e observabilidade dos sistemas construídos pelas equipes
 - Ajudar no recrutamento de novos engenheiros de software
 - Identificar oportunidades de melhorias e trabalhar com as equipes para implementá-las
 - Identificar e/ou avaliar tecnologias e produtos estratégicos para a empresa
 - Ser embaixador da Arquivei para fóruns tecnológicos diversos (externos e internos e diferentes níveis)
Indicadores:
 - Custo das soluções (desenvolvimento e manutenção) comparado contra soluções de mercado existentes
 - Tempo de desenvolvimento
 - Quantidade de bugs
 - Desempenho das aplicações (latência, memória, etc)
 - Soluções podem ser monitoradas
 - Apresenta anualmente em fóruns de tecnologia e/ou escreve artigos recorrentemente para o blog da empresa
** Atividades
#+begin_quote
Quais serão as principais tarefas, projetos e responsabilidades?
#+end_quote
- Tarefas:
  - Escrever e manter uma estratégia de engenharia  ([[https://staffeng.com/guides/engineering-strategy/][Writing engineering strategy | StaffEng]])
    - Escrever Design Documents
  - Mentorias técnicas e de desenvolvimento pessoal
  - Ajudar na priorização do roadmap técnico dos times
  - Desenhar ou ajudar a validar Arquiteturas
  - Ajudar as definir métricas de sucesso das aplicações
- Projetos:
  - Desenvolvimento de PoCs para validar hipóteses técnicas e de negócio
  - Apresentação de conteúdo:
    - Pitches
    - Mentorias
    - Vídeos
    - Artigos
    - Apresentações em fóruns de tecnologia
- Responsabilidades:
  - Avaliar a aplicabilidade das tendências de mercado na empresa
  - Foco em entregar dentro do prazo
  - Desenhar a melhor solução com menor custo possível
  - Analisar e destrinchar problemas antes de propor soluções
  - Eliminar incertezas e dúvidas do desenvolvimento de soluções
** Desempenho Padrão
#+begin_quote
O que é esperado desse papel em termos de desempenho?
#+end_quote
- Boa comunicação verbal e escrita
- Saber escrever boa documentação
- Saber diagramar/desenhar ideias e arquiteturas
- Estar atualizado com melhores práticas e tecnologias do mercado
- Saber conduzir provas de conceito
- Ser autoditada e pró-ativo
- Saber lidar com frustrações (PoCs dão errados, problemas complexos)
- Conhecimento avançado e/ou aplicado em Design Patterns, SOLID, Clean Code, CUPS
- Conhecer diversas Arquiteturas e saber quando usar cada uma
** Valores & Regras de Comportamento
#+begin_quote
Que atitudes e comportamentos são inaceitáveis e quais reforçam nossa Cultura e devo acompanhar de perto?
#+end_quote
Inaceitáveis:
- Atitudes contrárias ao DNA da empresa
- Iniciativas que não estão alinhadas com os objetivos da empresa
- Não saber trabalhar em equipe
- Não conseguir quebrar um problema em partes executáveis
- Aponta problemas sem sugestão de melhorias
- Não sinalizar problemas/riscos que poderiam ter sido antecipados
Reforçam:
- Discussões saudáveis sobre arquitetura, tecnologias, qualidade e melhores práticas
- Compartilhar material técnico com os outros engenheiros
- Incentivar pessoas a apresentarem soluções nos fóruns internos da empresa
- Ajudar a pessoas a apresentarem em eventos externos
- Se preocupar com a saúde e custo-benefício das soluções
- Estar disponível para as pessoas
- Ter uma visão sistemática dos problemas sendo resolvidos
- Entende como um sistema ou time afeta os sistemas ou times a sua volta
- Difundir soluções encontradas por um time para outros times
** Rituais
#+begin_quote
Quais cerimonias de gestão e acompanhamento são chave para desempenhar adequadamente o papel?
#+end_quote
1. *Reuniões de revisão de arquitetura*: regularmente revisando e atualizando a arquitetura dos sistemas da empresa, incluindo discutir pontos de dor, oportunidades de melhoria e aprimoramentos a serem feitos. Essas reuniões ajudam a garantir que os sistemas sejam escaláveis, confiáveis ​​e consistentes com a estratégia técnica da empresa.
2. *Revisões de código*: examinando o código dos engenheiros para garantir a qualidade, a segurança e a escalabilidade. As revisões de código também fornecem uma oportunidade para fornecer feedback e treinamento aos engenheiros.
3. *Reuniões individuais com a equipe*: encontrando-se com cada membro da equipe de engenharia regularmente para entender suas preocupações, necessidades e objetivos de carreira. Essas reuniões também são uma oportunidade para fornecer feedback e coaching individualizado.
4. *Reuniões de planejamento de projeto*: trabalhando com gerentes de produto e outros líderes de equipe para planejar projetos, definir objetivos e prioridades, estimar prazos e identificar possíveis obstáculos. Essas reuniões ajudam a garantir que a equipe esteja alinhada com as prioridades da empresa e trabalhando em projetos valiosos e realizáveis.
5. *Treinamento e mentoria*: liderando sessões de treinamento e mentoria para engenheiros de software, ajudando-os a desenvolver suas habilidades técnicas e de liderança.
6. *Participação em eventos e conferências*: representando a empresa em eventos do setor, palestras e conferências, apresentando palestras e compartilhando conhecimento técnico e melhores práticas.
7. *Reuniões de avaliação de desempenho*: trabalhando com gerentes de recursos humanos para revisar o desempenho da equipe e desenvolver planos de desenvolvimento pessoal para cada membro da equipe.

** Habilidades Chave
#+begin_quote
Quais habilidades são necessárias para realizar as atividades-chave - quais eu preciso olhar mais de perto, acompanhar e orientar?
#+end_quote
- Consegue comunicar ideias de forma clara
- Sabe escrever de documentação técnica
- Conhecer a fundo ao menos uma das linguagens de programação usadas pela a empresa
- Ser um bom líder
- Capacidade analítica
- Sabe correr riscos
- Trabalha bem em equipe
- Boa capacidade analítica
- Questionador
- Visão sistêmica
#+begin_quote
As habilidades chave que identificam um bom Staff Engineer incluem:

    Habilidade técnica: um bom Staff Engineer deve possuir habilidades técnicas sólidas em sua área de atuação. Eles devem ser capazes de lidar com problemas complexos, desenvolver soluções técnicas inovadoras e liderar a equipe na implementação de novas tecnologias.

    Pensamento estratégico: um bom Staff Engineer deve ser capaz de pensar estrategicamente e tomar decisões informadas que impulsionem a empresa a longo prazo. Eles devem ser capazes de avaliar riscos, antecipar mudanças e identificar oportunidades para melhorar o sistema e a eficiência da equipe.

    Habilidade de liderança: um bom Staff Engineer deve ser capaz de liderar a equipe e colaborar com outras partes interessadas para atingir objetivos comuns. Eles devem ser capazes de se comunicar efetivamente, fornecer feedback construtivo e desenvolver as habilidades da equipe.

    Capacidade de resolução de problemas: um bom Staff Engineer deve ser capaz de resolver problemas complexos e encontrar soluções técnicas inovadoras. Eles devem ser capazes de avaliar a situação, identificar a raiz do problema e implementar soluções eficazes.

    Habilidade de mentoria: um bom Staff Engineer deve ser capaz de fornecer mentoria e desenvolvimento para a equipe. Eles devem ser capazes de orientar a equipe, fornecer feedback construtivo e desenvolver as habilidades da equipe.

    Compreensão de negócios: um bom Staff Engineer deve ter uma compreensão sólida dos negócios e dos objetivos da empresa. Eles devem ser capazes de alinhar suas soluções técnicas com os objetivos de negócios e entender como suas soluções afetam o sucesso geral da empresa.

Essas habilidades são essenciais para um bom Staff Engineer e ajudam a garantir que eles possam liderar a equipe e fornecer soluções técnicas inovadoras que impulsionam a empresa a longo prazo. É importante lembrar que as habilidades podem variar dependendo da empresa e do setor em que atuam, mas essas habilidades fundamentais são essenciais para qualquer bom Staff Engineer.
-- chat GPT
#+end_quote
** Propósito
#+begin_quote
Por que fazemos o que estamos fazendo?
#+end_quote
Para habilitar a empresa a atacar problemas tecnicamente desafiadores, como processamento de dados eficiente com um alto volume de dados, e estar sempre a frente do mercado, utilizando metodologias e tecnologias de ponta que melhor superam os desafios de negócio.

#+begin_quote
O propósito de um Staff Engineer é fornecer liderança técnica em uma empresa ou organização. Eles existem para ajudar a empresa a resolver problemas técnicos complexos, implementar novas tecnologias e liderar a equipe técnica em direção aos objetivos de negócios.

Os Staff Engineers geralmente têm muita experiência e conhecimento em sua área de atuação e são capazes de ajudar a equipe a enfrentar desafios técnicos. Eles trabalham em estreita colaboração com outros líderes de equipe e partes interessadas da empresa para garantir que a equipe técnica esteja alinhada aos objetivos de negócios da empresa.

Além disso, um Staff Engineer é responsável por desenvolver e manter padrões técnicos na empresa, garantindo que a equipe técnica esteja trabalhando de maneira consistente e eficiente. Eles também podem ser responsáveis por avaliar e implementar novas tecnologias que possam ser benéficas para a empresa.

Em resumo, o propósito de um Staff Engineer é liderar e orientar a equipe técnica da empresa para alcançar objetivos técnicos e de negócios de longo prazo, garantindo que a empresa esteja usando as melhores práticas e tecnologias disponíveis.
-- chat GPT
#+end_quote

Por que gostamos de resolver problemas envolvendo alta volumetria de dados

-- Buzzword Bingo: Alta volumetria (grandes volumes de dados), facil de ser consumida.
