:PROPERTIES:
:ID:       27999cf3-a1bb-4784-9c50-90d47e204f13
:END:
#+title: Delve

- tags :: [[id:5bc1ac0b-6e1a-4250-8fae-03aa93d421b9][Golang]]

* What is?
A debugger for golang projects

* Using on docker-compose

We need to publish a new port for the debug server and start the debugger in a headless state.

#+begin_src yaml
version: '3.7'
services:
  myservice:
    ports:
      - 9001:9001
    entrypoint:
      - dlv
      - debug
      - ./cmd/api
      - --build-flags
      - "-gcflags='all=-N -l' -tags=musl -mod=vendor"
      - --headless
      - --listen
      - ":9001"
      - --api-version
      - "2"
      - --accept-multiclient
#+end_src

Then we can connect by cli using:

#+begin_src shell
dlv connect :9001
#+end_src

Or connect using an IDE, by executing ~dap-debug~ in emacs for example.

* Using the cli

The official guide is in [[https://github.com/go-delve/delve/blob/master/Documentation/cli/README.md][delve/README.md at master · go-delve/delve · GitHub]].

But the cli is self explanatory. just enter the command ~help~ to learn how to use it.

** Troubleshoot
*** File not found when using docker

This problem occurs because the debug server generates an absolute file path for the source but it refers to the file inside the container, and not on the host, where the cli is running.

You need to configure delve cli to substitute the path to fix the absolute file name from the container to the host.

Change ~substitute-path~ config in the config file ~~/.config/dlv/config.yml~:

#+begin_src
# Define sources path substitution rules. Can be used to rewrite a source path stored
# in program's debug information, if the sources were moved to a different place
# between compilation and debugging.
# Note that substitution rules will not be used for paths passed to "break" and "trace"
# commands.
substitute-path:
  # - {from: path, to: path}
  - { from: /go, to: /home/yourusername/go }
#+end_src

* Resources
- https://github.com/go-delve/delve
- https://www.jamessturtevant.com/posts/Using-the-Go-Delve-Debugger-from-the-command-line/
- https://golangforall.com/en/post/go-docker-delve-remote-debug.html
