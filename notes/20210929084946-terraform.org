:PROPERTIES:
:ID:       ba71ece8-ec56-4191-b896-a8a409568dc6
:END:
#+title: Terraform

- tags :: [[id:9acd3fa7-6259-4d0f-8cc3-01b87da979d9][Linux]] [[id:ae705197-91b7-4cc4-8f1c-39957e27d0a9][Cloud]]

* What is it?
[[https://www.terraform.io/][Terraform by HashiCorp]]

* Automating terraform
[[https://www.runatlantis.io/][Terraform Pull Request Automation | Atlantis]]
