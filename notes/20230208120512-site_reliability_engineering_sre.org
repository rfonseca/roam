:PROPERTIES:
:ID:       9ae1bbed-54a8-490e-abfc-bbfeb1fdf89d
:END:
#+title: Site Reliability Engineering (SRE)

* Links
- [[https://www.ibm.com/topics/site-reliability-engineering][What is Site Reliability Engineering (SRE)? | IBM]]
- [[https://github.com/bregman-arie/sre-checklist][GitHub - bregman-arie/sre-checklist: A checklist of anyone practicing Site Re...]]
