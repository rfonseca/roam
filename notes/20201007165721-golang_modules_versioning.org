:PROPERTIES:
:ID:       0f3bc1f9-7dda-4dff-8fb9-35e3e139ca2c
:END:
#+title: Golang Modules Versioning

- tags :: [[id:3bb8d854-0b2a-4856-bcb6-2c6c0f3b7b25][Semantic Versioning]] [[id:5bc1ac0b-6e1a-4250-8fae-03aa93d421b9][Golang]]

How to version a golang package?

Golang's blog has a [[https://blog.golang.org/v2-go-modules][post]] on the subject.

1. You must use semver;
2. Package must end in major version.
