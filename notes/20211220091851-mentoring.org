:PROPERTIES:
:ID:       3f612bcb-2a93-4aea-8f60-2b7c5f2bd28f
:END:
#+title: Mentoring

* What is it?
As a leader, help people reach their full potential and have a nice work-life balance.
** How things are done at buffer

[[https://buffer.com/resources/a-simple-guide-to-better-coaching-and-feedback-in-your-company/][A Simple Guide to Better Coaching and Feedback in Your Company]]

I Liked how they split their time:

1. Share and celebrate someone's achievements [10 min]
2. Discuss top challenges [40 min]
3. Leader gives feedback to someone [10 min]
4. Someone gives feedback to the leader [10 min]

I think that's too long but 60 min vs 10 min it's a good split that highlights the importance of the team.
