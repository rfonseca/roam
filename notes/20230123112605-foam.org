:PROPERTIES:
:ID:       c09dc24c-52c2-45b5-9b94-dc889d18a99e
:END:
#+title: Foam

- tags ::  [[id:80500fed-5ccf-4f5b-b08f-0d4a4826201b][Note taking]]

* What is it?

A note taking extention for VSCode similar to [[id:409424fc-0229-472c-90ad-010e3ae0d5e4][org roam]].

- https://foambubble.github.io/foam/
