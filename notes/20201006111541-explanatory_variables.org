:PROPERTIES:
:ID:       6591948b-5838-4f24-a231-85ff1d6be77a
:END:
#+title: Explanatory Variables

- tags :: [[id:18aeb48e-9860-4106-b5c5-e7d25f6a96f5][Clean Code]]

Use [[https://moderatemisbehaviour.github.io/clean-code-smells-and-heuristics/general/g19-use-explanatory-variables.html][intermetiate values]] to make the code more easily understandable.
