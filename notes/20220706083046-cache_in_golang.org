:PROPERTIES:
:ID:       b83a6a2e-1f62-470e-a9ea-59cc35d54f11
:END:
#+title: Cache in Golang

- tags :: [[id:5bc1ac0b-6e1a-4250-8fae-03aa93d421b9][Golang]] [[id:b2b00910-c177-4520-b9fa-831109b3a5ff][Cache]]

* GoCache

This is a cache library and allows multiple backends and even chaining backends together.

https://github.com/eko/gocache


* Go-Cache

This is a simple pure go in-memory caching with TTL.

https://github.com/patrickmn/go-cache

* Ristretto

Golang caching lib by Dgraph.

https://pkg.go.dev/github.com/dgraph-io/ristretto
