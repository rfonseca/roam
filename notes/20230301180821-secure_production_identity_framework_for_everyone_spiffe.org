:PROPERTIES:
:ID:       b983230f-babf-45c4-93b5-a2f0c705192a
:END:
#+title: #+title: Secure Production Identity Framework for Everyone (SPIFFE)

- tags :: [[id:9585fdca-fc98-401c-a54a-3bd37da79eac][Microservices]]

* What is it?
:PROPERTIES:
:ID:       bc0cc134-c87f-4edf-b7ba-0c22994d40cc
:END:
#+begin_quote
SPIFFE, the Secure Production Identity Framework For Everyone, provides a secure identity, in the form of a specially crafted X.509 certificate, to every workload in a modern production environment. SPIFFE removes the need for application-level authentication and complex network-level ACL configuration.
#+end_quote

- [[https://spiffe.io/][SPIFFE – Secure Production Identity Framework for Everyone]]

[[id:609fe4f4-fb88-437c-a7ab-d16b1f3d3291][Istio]] control plane uses this. Read more about it at [[https://istio.io/latest/docs/concepts/security][Istio / Security]].
