:PROPERTIES:
:ID:       fd98086b-9578-4eb2-91e3-2b1f40609854
:END:
#+title: Doom Emacs

- tags :: [[id:c358b0bb-1675-4976-934d-8328136742e7][Emacs]]

* What is it?
A configuration framework that is very fast and neat.
https://github.com/hlissner/doom-emacs

* Defining keybindings

https://rameezkhan.me/adding-keybindings-to-doom-emacs/

#+begin_src elisp
(map! :leader
      :desc "New journal entry"
      "a j j" #'org-journal-new-entry)

(map! :leader
      (:prefix-map ("a" . "applications")
       (:prefix ("j" . "journal")
        :desc "New journal entry" "j" #'org-journal-new-entry
        :desc "Search journal entry" "s" #'org-journal-search)))
#+end_src
