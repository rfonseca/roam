:PROPERTIES:
:ID:       bc046a81-85de-4284-be2a-57d2669884e5
:END:
#+title: Golang memory arenas

- tags :: [[id:5bc1ac0b-6e1a-4250-8fae-03aa93d421b9][Golang]] [[id:03efb795-b0c2-49cb-bb12-f1d9ac81235b][Golang Memory Management]]

* What is this

#+begin_quote
Memory arenas allow to allocate objects from a contiguous region of memory and free them all at once with minimal memory management or garbage collection overhead.
#+end_quote

* Links

- [[https://uptrace.dev/blog/golang-memory-arena.html][Golang memory arenas {101 guide}]]
