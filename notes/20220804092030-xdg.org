:PROPERTIES:
:ID:       51017c76-d133-4f61-927e-7da935d19251
:END:
#+title: XDG

- tags :: [[id:1a7afab0-612e-4835-a7a4-51664aee37cd][XDG Base Directory]] [[id:5bc1ac0b-6e1a-4250-8fae-03aa93d421b9][Golang]]

* What is this?
A go package that implements the XDG specification.
* Resurces
https://github.com/adrg/xdg
